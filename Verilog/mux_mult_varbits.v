module  mux_mult_varbits #(parameter lenghtA = 16, lenghtB = 16) (
    input [lenghtA-1:0] A,
    input [lenghtB-1:0] B,
    output wire[lenghtA+lenghtB-1:0] data_out
);
        wire [31:0] out1, out2, out3, out4, out5, out6, out7, out8;
        wire [31:0] temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8; 
    mux_mult cel1(
                .A      (A),
                .B      (B[1:0]),
		.out    (temp1)
            );
    mux_mult cel2(
                .A      (A),
                .B      (B[3:2]),
		.out    (temp2)
            );        
    mux_mult cel3(
                .A      (A),
                .B      (B[5:4]),
		.out    (temp3)
            );
    mux_mult cel4(
                .A      (A),
                .B      (B[7:6]),
		.out    (temp4)
            );
    mux_mult cel5(
                .A      (A),
                .B      (B[9:8]),
		.out    (temp5)
            );        
    mux_mult cel6(
                .A      (A),
                .B      (B[11:10]),
		.out    (temp6)
            );
    mux_mult cel7(
                .A      (A),
                .B      (B[13:12]),
		.out    (temp7)
            );        
    mux_mult cel8(
                .A      (A),
                .B      (B[15:14]),
		.out    (temp8)
            );

        assign out1=temp1;
        assign out2=temp2<<2; 
        assign out3=temp3<<4;
        assign out4=temp4<<6;
        assign out5=temp5<<8;
        assign out6=temp6<<10;
        assign out7=temp7<<12;
        assign out8=temp8<<14;

        assign data_out=out1+out2+out3+out4+out5+out6+out7+out8;

endmodule // mux_4b_mul
