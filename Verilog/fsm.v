module fsm (clk, reset, in_fact, go, fact, done);

    //Asignacion de estados 1 hot.

    parameter Start = 'b001;
    parameter Loop = 'b010;
    parameter Fin = 'b100;

    reg [2:0] state, nextstate;
    output reg [31:0] fact;
    output reg done;
    input wire reset;
    input wire clk;
    input wire go;
    input wire [3:0] in_fact;


    reg [4:0] counter = 0;
    reg [15:0] a, b;
    wire [31:0] c;
    reg [15:0] a_temp;
    reg [15:0] b_temp;
    reg [31:0] flopped_out;
    
    multiplicador mul (
                    //outputs
                    .data_out (c),
                    //inputs
                    .inA (a),
                    .inB (b)
                );

    always @(posedge clk) begin  // always block to update state
        if (~reset)begin
            state <= Start;
        end
        else begin
            state <= nextstate; 
            a_temp <= a;
            b_temp <= b;
            flopped_out <= c;
            if(state == Fin) counter=counter+1;
        end
    end


    always @(*) begin 
        nextstate=state;
        done = 0;
        case(state)
        Start: begin
            if(go==1) begin
                a = 1;
                b = in_fact; 
                nextstate = Loop;
            end
            else
                nextstate = Start;

        end

        Loop: begin
            if(b!=0) begin
                a=flopped_out;
                b=b_temp-1;
                nextstate = Loop;
            end

            else begin 
                nextstate = Fin;
            end
        end

        Fin: begin
            if (counter==31) begin
                nextstate=Start;
            end
            else begin
                fact=a;
                done = 1;      
            end
        end

        default:
            nextstate = Start;
        endcase
    end
endmodule