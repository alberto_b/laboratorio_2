module sumador(
    input inA,
    input inB,
    input carry,
    output out_Sum,
    output out_Carry);

assign {out_Carry, out_Sum} = inA + inB + carry;

endmodule // sumador con carry


