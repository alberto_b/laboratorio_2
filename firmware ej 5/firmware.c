#include <stdint.h>

uint32_t factorial (uint32_t i){
	if (i==1){
		return 1;
	}	
	else {
		return i*factorial(i-1);
	}
	
}

static void putufactorial(uint32_t i) {
	//*((volatile uint32_t *)0x10000000) = factorial(i);
	*((volatile uint32_t *)0x0FFFFFF0) = i;
	*((volatile uint32_t *)0x0FFFFFF4) = 1;
	if(*((volatile uint32_t *)0x0FFFFFFC) == 1){
		*((volatile uint32_t *)0x10000000) = *((volatile uint32_t *)0x0FFFFFF8);	
	}
	else
	{
		//*((volatile uint32_t *)0x0FFFFFFC) = 0;
	}
	
}

void main() {
	uint32_t number_to_display = 0;
	uint32_t counter = 0;
	uint32_t size = 6;
	uint32_t array[size];
	//*((volatile uint32_t *)0x0FFFFFF0) = 276;
	//*((volatile uint32_t *)0x0FFFFFF4) = 522;
	array[0]=2;
	array[1]=4;
	array[2]=6;
	array[3]=5;
	array[4]=1;
	array[5]=9;
	uint32_t sent = 0;	
	
	while (1) {
		counter = 0;
		number_to_display=0;
				
		while (number_to_display < size){
			
			if(sent == 0) {
				putufactorial(array[number_to_display]);
				number_to_display ++;
				sent = 1;
			}
			if(sent ==1) {
				*((volatile uint32_t *)0x0FFFFFF4) = 0;
			} 
				
			
			if(*((volatile uint32_t *)0x0FFFFFFC) != 1) {		
				
			} else {
				*((volatile uint32_t *)0x10000000) = *((volatile uint32_t *)0x0FFFFFF8);
				sent = 0;
			}
		}
	}
}
